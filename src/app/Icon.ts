export class Icon {
  name: string;
  svg: string;
  constructor(theName: string, theSvg: string) {
    this.name = theName;
    this.svg = theSvg;
  }
}
