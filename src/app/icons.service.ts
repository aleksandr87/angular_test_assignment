import { Injectable } from '@angular/core';
import {Icon} from './Icon';
import {BASIC_ICONS} from './shared/basic_icons';

@Injectable({
  providedIn: 'root'
})
export class IconsService {

  constructor() { }

  getIcons() {

    const icons: Icon[] = [];

    for (const [key, value] of Object.entries(BASIC_ICONS) ) {
      icons.push(new Icon(key, value));
    }
    return icons;
  }
}
