import {Component, Input, OnInit} from '@angular/core';
import {Icon} from '../../Icon';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {

  @Input()
  iconSize: string;
  @Input()
  icons: Icon[];
  @Input()
  color;
  private display = true;

  constructor() {
  }

  ngOnInit() {
    this.icons.sort(this.sorting);
  }

  // Функция сортировки по названию
  private sorting(a, b): number {
    return (a.name > b.name) ? 1 : -1;
  }

  // открытие и закрытие компонента accordion
  onAccClick(): void {
    this.display = !this.display;
  }
}
