import {Component, Input, OnInit} from '@angular/core';
import {IconsService} from '../icons.service';
import {Icon} from '../Icon';
import {SpinnerComponent} from '../shared/spinner/spinner.component';

@Component({
  selector: 'app-show-icons',
  templateUrl: './show-icons.component.html',
  styleUrls: ['./show-icons.component.scss']
})
export class ShowIconsComponent implements OnInit {

  // Массив всех иконок
  private icons: Icon[];
  // контейнер для хранения всех сгруппированных иконок
  allIconGroups: Map<string, Icon[]>;
  // контейнер для хранения сгруппированных иконок найденных по названию
  iconGroups: Map<string, Icon[]>;
  // цвет иконок
  color: string;
  loaded = false;

  constructor(private iconsService: IconsService) {
  }

  ngOnInit() {
    // Spinner включен
    this.loaded = false;
    // Создание массива иконок
    this.getIcons();
    // Группировка иконок по параметру width
    this.groupIcons(this.icons);

    // Выключение spinner-а
    // Специально установлено 2 сек, чтобы имитировать загрузку
    setTimeout(() => {
      this.loaded = true;
    }, 2000);
  }

  private getIcons(): void {
    // Чтение basic-icons.ts
    this.icons = this.iconsService.getIcons();
  }

  // Группировка иконок по параметру width
  private groupIcons(icons: Icon[]) {
    this.iconGroups = icons.reduce((res: Map<string, Icon[]>, a: Icon) => {
      const width = this.getWidth(a);
      if (!res.has(width)) {
        res.set(width, []);
      }
      res.get(width).push(a);
      return res;
    }, new Map<string, Icon[]>());
    // Все доступные иконки сохраняются в отдельном контейнере
    this.allIconGroups = new Map(this.iconGroups);
  }

  // определение параметра width иконки
  private getWidth(icon: Icon): string {
    const regexp = new RegExp('(width=")(\\d+)(")');
    const match = icon.svg.match(regexp);
    const width: string = (match !== null) ? match[2] : 'undefined'; // undefined - если width определить не удалось
    return width;
  }

  // Поиск иконок по названию
  getIconsBySearch(value: string) {
    if (value !== null) {
      for (const [k, v] of this.allIconGroups) {
        this.iconGroups.set(k, v.filter(icon => icon.name.includes(value)));
      }
    } else {
      this.iconGroups = this.allIconGroups;
    }
  }

  // раскрашивание в цвет по нажатию кнопки
  onChangeColor(color: string) {
    this.color = color;
  }
}
