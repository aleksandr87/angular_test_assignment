import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShowIconsComponent } from './show-icons/show-icons.component';
import { SanitizeHtmlPipe } from './utils/sanitize-html.pipe';
import {FormsModule} from '@angular/forms';
import {SpinnerComponent} from './shared/spinner/spinner.component';
import { AccordionComponent } from './show-icons/accordion/accordion.component';


@NgModule({
  declarations: [
    AppComponent,
    ShowIconsComponent,
    SanitizeHtmlPipe,
    SpinnerComponent,
    AccordionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
